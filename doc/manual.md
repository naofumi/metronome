# Main screen

## Normal mode

This is the default mode.

![Main screen in normal mode](1.png)

1. BPM: Floating-point values are allowed. There isn't any limit appart those of your phone and settings. ~0.2 to 120000 are possible on my phone
2. Bar: Floating-point bars are allowed. Every 1st beat is a "tic", everything else is a "toc". Ex:
	* 4: 4 beats bar
	* 1: 1 beat bar (only "tics")
	* 0: no bars (only "tocs")
	* 3.5: 7/8 groove
	* 2.66...: 3-3-2
	* 0.3333....: will play triolet
3. Tap In: A tap-in...
4. Mode: Select wich mode to use: "Normal" or the structure to play
5. Menu: Open the settings or the structure editor

## Structure mode

![Main screen in structure mode](2.png)

In structure mode:

1. the BPM now translates into a percentage to control every tempi at once. ex: BPM 200 means the structure is played twice the speed
2. the bar now sets the times the structure is to be repeated. **0 stands for ad lib**

# Settings menu

Choose wich sounds to use for "tic" and "toc" from real sounds (sticks sound good IMO), generated sounds ou a "muted" sound. If maybe you get lost with the possible values, just erase everything and follow the hint.

![Settings menu](5.png)

1. Spinner to choose the sound
2. If you chose a generated sound, those values represent the pitch in Hz and the length in milliseconds (0 means muted)
3. Sample Rate (Hz) (Do not Touch!) If the BPM limits doesn't suit you and if you know what you are doing, then adjust this. Only use with generated sounds

Here you can see "tic" set to a clap and "toc" is a generated E4 lasting 50ms.

# Structure menu

The idea is to define simple bar or simple elements as **bases** that you combine to build **patterns**, you obtain grouped bars or claves. Combining those patterns allow you to form **structures**.

Using the app ability to handle floating-point bars, adjusting bpms and "muting" the "toc", the structures allow lot of possibilities. The in-app demos are detailed below to illustrate thos possibilities.

![Structure menu](3.png) ![Structure menu](4.png)

1. Validate the modifications WITHOUT saving them to phone and return to main screen
2. Cancel the modifications et return to main screen
3. Write the modifications to the phone overwriting the last save
4. Open the structures saved on the phone overwriting those on-screen
5. Open a demo structure
6. Create a base
7. Create a pattern
8. Add a base to the concerned pattern
9. Create a structure
10. Add a pattern to the concerned structure

Use the trashbin to delete something.

The app offers 3 demos detailed below.

## Basic example

*All The Things You Are* version Gerald Clayton. 
The first 2 A are in 7 (4+3), the bridge is 6 (3+3) and the last A is 5 (3+2) for 12 bars.

Les 2 premiers A sont en 7 (4+3), le pont en 6 (3+3) et le dernier A en 5 (3+2) sur 12 mesures. quarter = quarter.

We could use bars with metric 7, 6 and 5. Instead, let's use composed bars:

* 3 bases: 4/4, 3/4 et 2/4
	* base1: 240 4
	* base2: 240 3
	* base3: 240 2
* 3 patterns: bar's metric 7/4, 6/4 et 5/4
	* pattern1: 4/4 + 3/4
		* base1 x 1
		* base2 x 1
	* pattern2: 3/4 x 2
		* base2 x 2
	* pattern3: 3/4 + 2/4
		* base2 x 1
		* base3 x 1
* 1 structure:
	* structure1: AABA'
		* pattern1 x 8
		* pattern2 x 4
		* pattern3 x 6

## Exemple d'équivalence (preset "All Things")

Il n'y a pas de moyen d'écrire explicitement une équivalence dans les structures. Cependant, on peut penser une équivalence comme un changement de tempo calculé: en appliquant une règle de trois entre l'équivalence et les bpms, on obtient l'effet souhaité.

On reprend l'exemple précédent mais en pensant à la blanche: 4/4 devient 2/2, 3/4 devient 2 noires pointées et 2/4 devient 1/2.

Il n'y a rien de compliqué pour passer à la blanche, on divise le bpm et la mesure par 2.  
Par contre, pour écrire les 2 noires pointées, on va penser "2 blanches mais plus courtes" càd 3/4 de leurs durées autrement dit avec le BPM (on inverse) valant les 4/3 de 120 -> 160.  
*(Si on veut penser en équivalence, la mesure à 2 noires pointées est en fait une mesure à 2/2 précédée d'une équivalence noire pointée devient blanche.)*

En bref, il n'y a  que les bases à changer:

* 3 bases: 2/2, 3/4 et 1/2
	* base1: 120 2 (2 blanches)
	* base2: 160 2 (2 noires pointés. 160 = 120*4/3)
	* base3: 120 1 (1 blanche)

**Conseils:** Pour faciliter les équivalences, partez d'un BPM facilement divisible par 2 (half-time) et 3 (équivalence triolet ou noire pointée) comme 90, 120, 180. Ça facilitera le calcul.

## Exemples Claves

Il est possible d'abuser du menu Structures pour lui faire jouer des figures rythmiques.

### Clave "Ewe" (preset "Ewe")

Pour les claves, on va se servir que de "tics" donc de mesures à 1 ou inférieur.

On va la penser (débit 12/8): noire, noire, croche, noire, noire, noire, croche:

* 2 bases: noire et croche
	* base1: 120 1 (noire)
	* base2: 120 0.5 (croche)
* 1 pattern:
	* pattern1:
		* base1 x 2
		* base2 x 1
		* base1 x 3
		* base2 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1

### Clave 2-3 (preset "2-3")

Dans certaines claves, on a besoin de silences. On va se servir de la mesure = 0 qui ne joue que des "tocs". On va ensuite dans les Réglages pour définir le "toc" comme son "Muet".

**Ne pas oublier de "démuter" le "toc" après.**

* 4 bases: noire, croche pointée, croche et demi-soupir (croche muette)
	* base1: 120 1
	* base2: 120 0.75 ou 160 1
	* base3: 120 0.5 ou 240 1
	* base4: 240 0 (mesure à 0 joue que des tocs, ici une croche)
* 1 pattern:
	* pattern1:
		* base4 x 1
		* base3 x 1
		* base1 x 1
		* base2 x 2
		* base3 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1