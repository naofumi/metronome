# Écran principal

## Mode Normal

C'est le mode par défaut.

![Écran principal normal](1.png)

1. BPM: Valeurs décimales possibles. Il n'y a pas vraiment de limites à part celles définies par la mémoire du téléphone et le réglages choisis. ~0.2 à 120000 chez moi
2. Mesure: On peut mettre des mesures à virgules. Le premier temps de chaque mesure est un "tic", le reste "toc". Ex:
	* 4: mesure à 4 temps
	* 1: mesure à 1 temps (que des "tics")
	* 0: pas de mesure (que des "tocs")
	* 3.5: groove 7/8 pensé à la noire (3 noires + 1 croche)
	* 2.66...: 3-3-2 (2 noires pointées + 1 noire)
	* 0.3333....: jouera un débit de triolet de croche
3. Tap In: Un tap-in...
4. Mode: Permet de choisir le mode: "Normal" ou bien la structure à jouer
5. Menu: Pour aller aux réglages des sons ou à l'édition des structures

## Mode Structure

![Écran principal structure](2.png)

En mode structure:

1. le BPM devient un pourcentage qui permet d'ajuster tout les tempi présents dans la structure. ex: BPM à 200 jouera 2 fois plus vite
2. la mesure définit ici le nombre de fois que doit être jouée la structure. **Ici 0 veut dire ad lib**

# Menu Réglages

Permet de choisir les sons à utiliser pour le "tic" et le "toc" parmi différents sons réels (les sticks sont pas mal), sons générés ou un son "muet". Si jamais vous ne savez plus quoi mettre, effacez tout, la valeur conseillée apparait alors en suggestion.

![Menu réglages](5.png)

1. Menu déroulant pour choisir le son
2. Si un son généré est choisi, ces valeurs permettent de choisir la hauteur du son du son en Hertz ainsi que sa durée en millisecondes (0 veut dire "muet")
3. Fréq. échantillonage (Hz) (Ne pas Toucher!) Si les limites du BPM ne conviennent pas et si on sait ce qu'on fait, on peut ajuster cette valeur. À utiliser uniquement avec des sons générés

Ici, un clap pour "tic" est un son généré (un Mi4 de 50ms) pour "toc".

# Menu Structures

L'idée est de définir des mesures ou éléments de **base** que l'on combine en **patterns**, ça donne des groupes de mesures ou des claves. On combine ensuite ces patterns pour former des **structures**.

En se servant de la possibilité d'écrire des mesures non-entières, en ajustant les bpms entre eux et en "mutant" le "toc", les structures offrent en fait pas mal de possibilités. Les démos de structure présentes dans l'appli sont détaillées plus loin afin de présenter ces possibilités.

![Menu structure](3.png) ![Menu structure](4.png)

1. Valide les modifications SANS les sauvegarder sur le téléphone et retourne à l'écran principal
2. Annule les modifications et retourne à l'écran principal
3. Sauvegarde les modifications sur le téléphone en écrasant l'ancien fichier
4. Ouvre les structures enregistrées sur le téléphone en écrasant celles à l'écran
5. Ouvre une structure de démo
6. Crée un élément de base
7. Crée un pattern
8. Ajoute une base au pattern concerné
9. Crée une structure
10. Ajoute un pattern à la structure concernée

Les corbeilles permettent de supprimer les éléments en trop.

L'appli propose 3 exemples de structure qui sont détaillés plus bas.

## Exemple basique

*All The Things You Are* version Gerald Clayton.  
Les 2 premiers A sont en 7 (4+3), le pont en 6 (3+3) et le dernier A en 5 (3+2) sur 12 mesures. Il n'y a pas d'équivalence, c'est noire = noire.

On pourrait simplement utiliser une base en 7, une en 6 et une en 5 mais on va plutôt composer les mesures:

* 3 bases: 4/4, 3/4 et 2/4
	* base1: 240 4
	* base2: 240 3
	* base3: 240 2
* 3 patterns: mesure en 7/4, 6/4 et 5/4
	* pattern1: 4/4 + 3/4
		* base1 x 1
		* base2 x 1
	* pattern2: 3/4 x 2
		* base2 x 2
	* pattern3: 3/4 + 2/4
		* base2 x 1
		* base3 x 1
* 1 structure:
	* structure1: AABA'
		* pattern1 x 8
		* pattern2 x 4
		* pattern3 x 6

## Exemple d'équivalence (preset "All Things")

Il n'y a pas de moyen d'écrire explicitement une équivalence dans les structures. Cependant, on peut penser une équivalence comme un changement de tempo calculé: en appliquant une règle de trois entre l'équivalence et les bpms, on obtient l'effet souhaité.

On reprend l'exemple précédent mais en pensant à la blanche: 4/4 devient 2/2, 3/4 devient 2 noires pointées et 2/4 devient 1/2.

Il n'y a rien de compliqué pour passer à la blanche, on divise le bpm et la mesure par 2.  
Par contre, pour écrire les 2 noires pointées, on va penser "2 blanches mais plus courtes" càd 3/4 de leurs durées autrement dit avec le BPM (on inverse) valant les 4/3 de 120 -> 160.  
*(Si on veut penser en équivalence, la mesure à 2 noires pointées est en fait une mesure à 2/2 précédée d'une équivalence noire pointée devient blanche.)*

En bref, il n'y a  que les bases à changer:

* 3 bases: 2/2, 3/4 et 1/2
	* base1: 120 2 (2 blanches)
	* base2: 160 2 (2 noires pointés. 160 = 120*4/3)
	* base3: 120 1 (1 blanche)

**Conseils:** Pour faciliter les équivalences, partez d'un BPM facilement divisible par 2 (half-time) et 3 (équivalence triolet ou noire pointée) comme 90, 120, 180. Ça facilitera le calcul.

## Exemples Claves

Il est possible d'abuser du menu Structures pour lui faire jouer des figures rythmiques.

### Clave "Ewe" (preset "Ewe")

Pour les claves, on va se servir que de "tics" donc de mesures à 1 ou inférieur.

On va la penser (débit 12/8): noire, noire, croche, noire, noire, noire, croche:

* 2 bases: noire et croche
	* base1: 120 1 (noire)
	* base2: 120 0.5 (croche)
* 1 pattern:
	* pattern1:
		* base1 x 2
		* base2 x 1
		* base1 x 3
		* base2 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1

### Clave 2-3 (preset "2-3")

Dans certaines claves, on a besoin de silences. On va se servir de la mesure = 0 qui ne joue que des "tocs". On va ensuite dans les Réglages pour définir le "toc" comme son "Muet".

**Ne pas oublier de "démuter" le "toc" après.**

* 4 bases: noire, croche pointée, croche et demi-soupir (croche muette)
	* base1: 120 1
	* base2: 120 0.75 ou 160 1
	* base3: 120 0.5 ou 240 1
	* base4: 240 0 (mesure à 0 joue que des tocs, ici une croche)
* 1 pattern:
	* pattern1:
		* base4 x 1
		* base3 x 1
		* base1 x 1
		* base2 x 2
		* base3 x 1
* 1 structure:
	* structure1:
		* pattern1 x 1