/** \file MenuStructures.java */

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;
import android.os.Build;//For conditioning color use depending on device's API

import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;

import android.content.Intent;//For passing data with Metronome

//import android.util.Log;

/** There must a cleaner way to achieve the same result... design-wise...
* In order to understand how the layout are nested, a drawing is much needed
*/
public class MenuStructures extends Activity
{

	StructureContainer container;
	List<String> listBase = new ArrayList<String>();
	List<String> listPattern = new ArrayList<String>();

	ArrayAdapter<String> adapterSpinnerBase;//We can't instantiate before activity is set up.
	ArrayAdapter<String> adapterSpinnerPattern;

	private Random r = new Random();
	private String FILENAME = "structures";
	private Spinner spinnerPresets;
	private boolean isSpinnerPresetsListening = false;//Sinon, le onItemSelected s'enclenche pendant onCreate

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menustructures);

		/* On instantie les adapter et on modifie getView pour y mettre les couleurs */
		adapterSpinnerBase = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listBase) {
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View v = super.getView(position,convertView,parent);

				TextView baseName = (TextView)((LinearLayout)((RelativeLayout)((LinearLayout)findViewById(R.id.menuStructuresLinearLayoutBases)).getChildAt(position)).getChildAt(0)).getChildAt(0);

				((TextView)v).setTextColor(baseName.getCurrentTextColor());
				if (Build.VERSION.SDK_INT > 11)//getColor() is API 11 minimum (honeycomb) so we use background color in spinner only of >11
					((TextView)v).setBackgroundColor(((ColorDrawable)baseName.getBackground()).getColor());
				return v;
			}
		};
		adapterSpinnerBase.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		/* On instantie les adapter et on modifie getView pour y mettre les couleurs */
		adapterSpinnerPattern = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listPattern) {
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View v = super.getView(position,convertView,parent);

				TextView patternName = (TextView)((LinearLayout)((RelativeLayout)((LinearLayout)((LinearLayout)findViewById(R.id.menuStructuresLinearLayoutPatterns)).getChildAt(position)).getChildAt(0)).getChildAt(0)).getChildAt(0);

				((TextView)v).setTextColor(patternName.getCurrentTextColor());
				if (Build.VERSION.SDK_INT > 11)
					((TextView)v).setBackgroundColor(((ColorDrawable)patternName.getBackground()).getColor());
				return v;
			}
		};
		adapterSpinnerPattern.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		/* Spinner des presets */
		Spinner spinnerPresets = (Spinner) findViewById(R.id.menuStructuresSpinnerPresets);
		ArrayAdapter<CharSequence> adapterPresets = ArrayAdapter.createFromResource(this,
		R.array.menuStructuresListPresets, android.R.layout.simple_spinner_item);
		adapterPresets.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPresets.setAdapter(adapterPresets);
		spinnerPresets.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
    			public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    				//Le listener reçoit un signal à blanc pendant le onCreate (le dialog s'affiche à chaque fois que l'activité est ouverte), donc on le zappe la première fois et on modifie le "flag" pour la suite.
    				if (!isSpinnerPresetsListening) {
	    				isSpinnerPresetsListening = true;
    					return;
    				}
    				
				AlertDialog.Builder builder = new AlertDialog.Builder(MenuStructures.this);
				final int demoChoice = position;//Pour y a voir accés dans le onClick du dialog
				builder.setTitle(R.string.menuStructuresDialogPresets_title).setMessage(getResources().getString(R.string.menuStructuresDialogPresets_message));
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick (DialogInterface dialog, int id) {		
						switch (demoChoice) {// Passer par un instance de container puis appeler setViewFromContainer et plus pratique que de directement remplir View mais ça reviendrait au même (le container est tjrs resynchronisé à la View avant d'être envoyer dans intent ou sauvegardé)
							case 1:
								container.initAllThings();
								break;
							case 2:
								container.initEwe();
								break;
							case 3:
								container.init23();
								break;
						}
						setViewFromContainer(container);
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick (DialogInterface dialog, int id) {
					}
				});

				AlertDialog dialog = builder.create();
				dialog.show();
    			}
    			@Override
	    		public void onNothingSelected(AdapterView<?> arg0) {
	    		}
	    	});

		Intent intent = getIntent();
		container = (StructureContainer)intent.getSerializableExtra("Container");
		setViewFromContainer(container);
	}

	/** Convenient method to start fresh */
	private void resetUI ()
	{
		LinearLayout layout;
		layout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutStructures);
		layout.removeAllViews();
		layout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutPatterns);
		layout.removeAllViews();
		layout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		layout.removeAllViews();

		listBase.clear();
		listPattern.clear();
	}
	
	private void setViewFromContainer (StructureContainer container)
	{
		/* Prepare UI */
		resetUI();

		for(int i=0;i<container.bases.length;i++) {
			RelativeLayout baseLayout = buildBase();
			((EditText)((LinearLayout)baseLayout.getChildAt(0)).getChildAt(1)).setText(String.valueOf(container.bases[i].bpm));
			((EditText)((LinearLayout)baseLayout.getChildAt(0)).getChildAt(2)).setText(String.valueOf(container.bases[i].bar));
		}
		for(int i=0;i<container.patterns.length;i++) {
			LinearLayout patternLayout = buildPattern();
			for(int j=0;j<container.patterns[i].bases.length;j++) {
				RelativeLayout patternBaseLayout = buildPatternBase(patternLayout.getChildAt(1));
				((Spinner)((LinearLayout)patternBaseLayout.getChildAt(0)).getChildAt(0)).setSelection(container.patterns[i].bases[j].index);
				((EditText)((LinearLayout)patternBaseLayout.getChildAt(0)).getChildAt(2)).setText(String.valueOf(container.patterns[i].bases[j].times));
			}
		}
		for(int i=0;i<container.structures.length;i++) {
			LinearLayout structureLayout = buildStructure();
			for(int j=0;j<container.structures[i].patterns.length;j++) {
				RelativeLayout structurePatternLayout = buildStructurePattern(structureLayout.getChildAt(1));
				((Spinner)((LinearLayout)structurePatternLayout.getChildAt(0)).getChildAt(0)).setSelection(container.structures[i].patterns[j].index);
				((EditText)((LinearLayout)structurePatternLayout.getChildAt(0)).getChildAt(2)).setText(String.valueOf(container.structures[i].patterns[j].times));
			}
		}
	}

	/** Check for missing values and use of non-existent element (avoid inconsistent structure), fill the container and return true if everything is fine, false otherwise */
	private boolean setContainerFromView ()
	{
		/* Prepare Data */
		container = new StructureContainer();

		/* Bases */
		LinearLayout basesLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		int basesCount = basesLayout.getChildCount();
		container.bases = new Base[basesCount];
		for(int i=0;i<basesCount;i++) {
			RelativeLayout baseLayout = (RelativeLayout)basesLayout.getChildAt(i);
			container.bases[i] = new Base();


			String bpm = ((EditText)((LinearLayout)baseLayout.getChildAt(0)).getChildAt(1)).getText().toString();
			String bar = ((EditText)((LinearLayout)baseLayout.getChildAt(0)).getChildAt(2)).getText().toString();
			if (bpm.matches("") || bar.matches("")) {
				Toast.makeText(getApplicationContext(),R.string.menuStructuresToastBaseWarning,Toast.LENGTH_SHORT).show();
				return false;
			}
			if (Double.parseDouble(bpm) <= 0) {
				Toast.makeText(getApplicationContext(),R.string.toastBpmCantBeZero,Toast.LENGTH_SHORT).show();
				return false;
			}
			container.bases[i].bpm = Double.parseDouble(bpm);
			container.bases[i].bar = Double.parseDouble(bar);
		}
		
		/* Patterns */
		LinearLayout patternsLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutPatterns);
		int patternsCount = patternsLayout.getChildCount();
		container.patterns = new Pattern[patternsCount];
		for(int i=0;i<patternsCount;i++) {
			LinearLayout patternLayout = (LinearLayout)patternsLayout.getChildAt(i);
			container.patterns[i] = new Pattern();

			int patternBasesCount = ((LinearLayout)patternLayout.getChildAt(1)).getChildCount();
			container.patterns[i].bases = new Pattern.Base[patternBasesCount];
			for(int j=0;j<patternBasesCount;j++) {
				RelativeLayout patternBaseLayout = (RelativeLayout)((LinearLayout)patternLayout.getChildAt(1)).getChildAt(j);
				container.patterns[i].bases[j] = new Pattern.Base();
				int position = ((Spinner)((LinearLayout)patternBaseLayout.getChildAt(0)).getChildAt(0)).getSelectedItemPosition();
				if (position >= 0 && position < basesCount)//Je n'arriva pas à trouver la constante INVALID_POSITION (=-1), ça serait plus simple
					container.patterns[i].bases[j].index = position;
				else {
					Toast.makeText(getApplicationContext(),R.string.menuStructuresToastPatternNoBase,Toast.LENGTH_SHORT).show();
					return false;
				}
				String times = ((EditText)((LinearLayout)patternBaseLayout.getChildAt(0)).getChildAt(2)).getText().toString();
				if (!times.matches(""))
					container.patterns[i].bases[j].times = Integer.parseInt(times);
				else {
					Toast.makeText(getApplicationContext(),R.string.menuStructuresToastPatternNoTimes,Toast.LENGTH_SHORT).show();
					return false;
				}
			}
		}

		/* Structures */
		LinearLayout structuresLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutStructures);
		int structuresCount = structuresLayout.getChildCount();
		container.structures = new Structure[structuresCount];
		for(int i=0;i<structuresCount;i++) {
			LinearLayout structureLayout = (LinearLayout)structuresLayout.getChildAt(i);
			container.structures[i] = new Structure();

			int structurePatternsCount = ((LinearLayout)structureLayout.getChildAt(1)).getChildCount();
			container.structures[i].patterns = new Structure.Pattern[structurePatternsCount];
			for(int j=0;j<structurePatternsCount;j++) {
				RelativeLayout structurePatternLayout = (RelativeLayout)((LinearLayout)structureLayout.getChildAt(1)).getChildAt(j);
				container.structures[i].patterns[j] = new Structure.Pattern();
				int position = ((Spinner)((LinearLayout)structurePatternLayout.getChildAt(0)).getChildAt(0)).getSelectedItemPosition();
				if (position >= 0 && position < patternsCount)
					container.structures[i].patterns[j].index = position;
				else {
					Toast.makeText(getApplicationContext(),R.string.menuStructuresToastStructureNoPattern,Toast.LENGTH_SHORT).show();
					return false;
				}
				String times = ((EditText)((LinearLayout)structurePatternLayout.getChildAt(0)).getChildAt(2)).getText().toString();
				if (!times.matches(""))
					container.structures[i].patterns[j].times = Integer.parseInt(times);
				else {
					Toast.makeText(getApplicationContext(),R.string.menuStructuresToastStructureNoTimes,Toast.LENGTH_SHORT).show();
					return false;
				}
			}
		}
		
		return true;

		/* LOG FOR TESTING AND TRANSPARENCY */
		/*
		for(int i=0;i<basesCount;i++) {
			Log.w("djksqjd","Base "+i+(container.bases[i].bpm)+(container.bases[i].bar));
		}

		for(int i=0;i<patternsCount;i++) {
			for(int j=0;j<container.patterns[i].bases.length;j++) {
				Pattern.Base base = container.patterns[i].bases[j];
				Log.w("djksqjd","Pattern "+i+(base.index)+(base.times));
			}
		}
		for(int i=0;i<intentStructure.size();i++) {
			Structure structure = intentStructure.get(i);
			for(int j=0;j<structure.patterns.length;j++) {
				Structure.Pattern pattern = structure.patterns[j];
				Log.w("djksqjd","Structure "+i+(pattern.index)+(pattern.times));
			}
		}*/
	}


	/* User Input */

	/** Rename bases's textviews and reorganize spinner when a base is deleted */
	void updateBases (int n)
	{//Rename all the next base (base textview and patternbasespinner) by decreasing the shown index.
		LinearLayout basesLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		listBase.remove(n);
		for(int i=n;i<basesLayout.getChildCount();i++) {
			((TextView)((LinearLayout)((RelativeLayout)basesLayout.getChildAt(i)).getChildAt(0)).getChildAt(0)).setText(String.format(getResources().getString(R.string.menuStructuresBase)+" %1$d",i+1));
			listBase.set(i,String.format(getResources().getString(R.string.menuStructuresBase)+" %1$d",i+1));
		}
	}

	/** Rename patterns's textviews and reorganize spinner when a pattern is deleted */
	void updatePatterns (int n)
	{
		LinearLayout patternsLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutPatterns);
		listPattern.remove(n);
		for(int i=n;i<patternsLayout.getChildCount();i++) {
			((TextView)((LinearLayout)((RelativeLayout)((LinearLayout)patternsLayout.getChildAt(i)).getChildAt(0)).getChildAt(0)).getChildAt(0)).setText(String.format(getResources().getString(R.string.menuStructuresPattern)+" %1$d",i+1));
			listPattern.set(i,String.format(getResources().getString(R.string.menuStructuresPattern)+" %1$d",i+1));
		}
	}

	/** Append a new baseLayout to the basesLayout and add a spinner entry */
	public RelativeLayout buildBase () {
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout baseLayout = (RelativeLayout)inflater.inflate(R.layout.menustructuresbase,null);

		//On l'ajoute et on rempli le textview par rapport à la position dans baseslayout
		LinearLayout basesLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		basesLayout.addView(baseLayout);

		String name = new String(getResources().getString(R.string.menuStructuresBase)+" "+(basesLayout.indexOfChild(baseLayout)+1));
		float[] hsvText = new float[]{(float) r.nextInt(360),100.f,100.f};
		float[] hsvBackground = new float[3];
		hsvBackground[0] = (hsvText[0]+180)%360;//Back is opposed to text
		hsvBackground[1] = 100.f;
		hsvBackground[2] = 50.f;
		TextView baseName = (TextView)((LinearLayout)baseLayout.getChildAt(0)).getChildAt(0);
		baseName.setText(name);
		baseName.setTextColor(Color.HSVToColor(hsvText));
		baseName.setBackgroundColor(Color.HSVToColor(hsvBackground));
		//On rajoute une entrée pour les spinners
		listBase.add(name);

		return baseLayout;
	}

	/** UI callback */
	public void onCreateBase (View v) {
		buildBase();
	}

	/** UI callback */
	public void onDeleteBase (View v)
	{
		int i;

		LinearLayout basesLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		i = basesLayout.indexOfChild((View) v.getParent());
		basesLayout.removeView((View) v.getParent());

		updateBases(i);
	}

	//PATTERNS
	public LinearLayout buildPattern ()
	{
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout patternLayout = (LinearLayout)inflater.inflate(R.layout.menustructurespattern,null);

		//On l'ajoute et on rempli le textview par à la position dans layout
		LinearLayout patternsLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutPatterns);
		patternsLayout.addView(patternLayout);

		String name = new String(getResources().getString(R.string.menuStructuresPattern)+" "+(patternsLayout.indexOfChild(patternLayout)+1));
		float[] hsvText = new float[]{(float) r.nextInt(360),100.f,100.f};
		float[] hsvBackground = new float[3];
		hsvBackground[0] = (hsvText[0]+180)%360;//Back is opposed to text
		hsvBackground[1] = 100.f;
		hsvBackground[2] = 50.f;
		TextView patternName = (TextView)((LinearLayout)((RelativeLayout)patternLayout.getChildAt(0)).getChildAt(0)).getChildAt(0);
		patternName.setText(name);
		patternName.setTextColor(Color.HSVToColor(hsvText));
		patternName.setBackgroundColor(Color.HSVToColor(hsvBackground));
		//On ajoute l'entrée pour les spinners
		listPattern.add(name);

		return(patternLayout);
	}
	public void onCreatePattern (View v) {
		buildPattern();
	}

	public void onDeletePattern (View v)
	{
		int i;

		LinearLayout patternsLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutPatterns);
		i = patternsLayout.indexOfChild((View)((View)v.getParent()).getParent());
		patternsLayout.removeView((View)((View)v.getParent()).getParent());

		updatePatterns(i);
	}

	/** Append a patternBaseLayout to the concerned patternLayout v */
	public RelativeLayout buildPatternBase (View v)
	{
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout patternBaseLayout = (RelativeLayout)inflater.inflate(R.layout.menustructurespatternbase,null);

		//On l'ajoute et on rempli le spinner
		((LinearLayout)v).addView(patternBaseLayout);
		((Spinner)((LinearLayout)patternBaseLayout.getChildAt(0)).getChildAt(0)).setAdapter(adapterSpinnerBase);

		return(patternBaseLayout);
	}

	/** UI callback we first find the pattern concerned then call buildPatternBase for him */
	public void onAddPatternBase (View v) {
		LinearLayout patternBasesLayout = ((LinearLayout)((LinearLayout)((RelativeLayout)((LinearLayout)v.getParent()).getParent()).getParent()).getChildAt(1));//On part du boutton, on remonte au > layout header du pattern > layout du pattern > on redescend dans le layout prévu au patternbase.
		buildPatternBase(patternBasesLayout);
	}

	public void onDeletePatternBase (View v)
	{
		LinearLayout patternBasesLayout = ((LinearLayout)((RelativeLayout)v.getParent()).getParent());
		patternBasesLayout.removeView((View) v.getParent());
	}

	//Structure
	public LinearLayout buildStructure ()
	{
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout structureLayout = (LinearLayout)inflater.inflate(R.layout.menustructuresstructure,null);

		//On l'ajoute et on rempli le textview par à la position dans layout
		LinearLayout structuresLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutStructures);
		structuresLayout.addView(structureLayout);

		String name = new String(getResources().getString(R.string.menuStructuresStructure)+" "+(structuresLayout.indexOfChild(structureLayout)+1));
		float[] hsvText = new float[]{(float) r.nextInt(360),100.f,100.f};
		float[] hsvBackground = new float[3];
		hsvBackground[0] = (hsvText[0]+180)%360;//Back is opposed to text
		hsvBackground[1] = 100.f;
		hsvBackground[2] = 50.f;
		TextView structureName = (TextView) ((LinearLayout)((RelativeLayout)structureLayout.getChildAt(0)).getChildAt(0)).getChildAt(0);
		structureName.setText(name);
		structureName.setTextColor(Color.HSVToColor(hsvText));
		structureName.setBackgroundColor(Color.HSVToColor(hsvBackground));

		return(structureLayout);
	}
	public void onCreateStructure (View v) {
		buildStructure();
	}

	public void onDeleteStructure (View v)
	{
		int n;

		LinearLayout structuresLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutStructures);
		n = structuresLayout.indexOfChild((View)((View)v.getParent()).getParent());
		structuresLayout.removeView((View)((View)v.getParent()).getParent());

		for(int i=n;i<structuresLayout.getChildCount();i++)
			((TextView)((LinearLayout)((RelativeLayout)((LinearLayout)structuresLayout.getChildAt(i)).getChildAt(0)).getChildAt(0)).getChildAt(0)).setText(String.format(getResources().getString(R.string.menuStructuresBase)+" %1$d",i+1));
	}

	public RelativeLayout buildStructurePattern (View v)
	{
		//On inflate
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout structurePatternLayout = (RelativeLayout)inflater.inflate(R.layout.menustructuresstructurepattern,null);

		//On l'ajoute et on rempli le spinner
		((LinearLayout)v).addView(structurePatternLayout);
		((Spinner)((LinearLayout)structurePatternLayout.getChildAt(0)).getChildAt(0)).setAdapter(adapterSpinnerPattern);

		return(structurePatternLayout);
	}
	public void onAddStructurePattern (View v) {
		LinearLayout structurePatternsLayout = ((LinearLayout)((LinearLayout)((RelativeLayout)((LinearLayout)v.getParent()).getParent()).getParent()).getChildAt(1));//On part du boutton, on remonte au layout du structure puis on redescend dans le layout prévu au structurepattern.
		buildStructurePattern(structurePatternsLayout);
	}

	public void onDeleteStructurePattern (View v)
	{
		LinearLayout structurePatternsLayout = ((LinearLayout)((RelativeLayout)v.getParent()).getParent());
		structurePatternsLayout.removeView((View) v.getParent());
	}

	public void onMenuStructuresOk (View v)
	{
		if (setContainerFromView()) {
			Intent intent = new Intent();
			intent.putExtra("Container",container);
			setResult(1,intent);
			finish();
		}
	}

	public void onMenuStructuresCancel (View v)
	{
		setResult(0,null);
		finish();
	}
	
	public void onMenuStructuresSave (View v)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuStructures.this);
		builder.setTitle(R.string.menuStructuresDialogSave_title).setMessage(R.string.menuStructuresDialogSave_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				if (setContainerFromView())
					container.saveToInternal(getApplicationContext());
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public void onMenuStructuresLoad (View v)
	{
		LinearLayout basesLayout = (LinearLayout) findViewById(R.id.menuStructuresLinearLayoutBases);
		AlertDialog.Builder builder = new AlertDialog.Builder(MenuStructures.this);
		builder.setTitle(R.string.menuStructuresDialogLoad_title).setMessage(R.string.menuStructuresDialogLoad_message);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
				StructureContainer instance = new StructureContainer();
				container = instance.loadFromInternal(getApplicationContext());
				setViewFromContainer(container);
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			public void onClick (DialogInterface dialog, int id) {
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
