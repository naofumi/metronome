/** \file Clicker.java */

package com.arnaud.metronome;

import android.content.Context;
import android.content.SharedPreferences;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

//import android.util.Log;

/** The worker class */
public class Clicker
{
	private double bpm;
	private double bar;
	private float ticFreq, tocFreq;
	private int ticChoice, tocChoice, ticLength, tocLength; // ticLength is how long the sound last before silence
	private boolean isRunning; /**< to control the while loop from outside the Task because canceling the task won't kill the loop */
	
	private int sampleRate;
	private byte [] tic;
	private byte [] toc;
	private AudioTrack audioTrack;
	
	public void on(double bpm, double bar, Context context)
	{
		SharedPreferences settings = context.getSharedPreferences("settings", 0);
		this.ticChoice = settings.getInt("tic",0);
		this.tocChoice = settings.getInt("toc",0);
		//this.ticOffset = settings.getInt("ticOffset",0);
		this.ticFreq = settings.getFloat("ticFreq",880);
		this.ticLength = settings.getInt("ticLength",50);
		//this.tocOffset = settings.getInt("tocOffset",0);
		this.tocFreq = settings.getFloat("tocFreq",660);
		this.tocLength = settings.getInt("tocLength",50);
		this.sampleRate = settings.getInt("sampleRate",44100);
		
		this.bpm = bpm;
		this.bar = bar;
		
		tic = Signal.getPCM(context,ticChoice,ticFreq,ticLength,sampleRate,bpm);
		toc = Signal.getPCM(context,tocChoice,tocFreq,tocLength,sampleRate,bpm);
		
		isRunning = false;
		
		/*Audio init*/
		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, 2*sampleRate, AudioTrack.MODE_STREAM); /**< Buffer is set twice the size of sampleRate so the loop will always be feeding the buffer with 2sec buffered */
		audioTrack.play();
	}
	
	public void off()
	{
		audioTrack.stop();
		audioTrack.release();
	}
	
	public void setIsRunning (boolean isRunning)
	{
		this.isRunning = isRunning;
	}
	
	/** Actual worker typically used through an android Task */
	public void run()
	{
		int beat = 0;
		
		/** In order: from most common to useless. Do not change the order without adaptating conditions<br>
		* bar is int > 1 tic+tocs<br>
		* bar is dec > 1 tic+tocs+partial toc<br>
		* bar = 1 tic<br>
		* bar = 0 toc<br>
		* bar ]0,1[ stupid partial tic
		*/
		
		if (bar%1 == 0. && bar != 0)//bar is "int". Common use.
			while(isRunning) {
				beat %= (int) bar;//Find which beat is played and Avoid possible int flow.
				//Log.w("metronome_clicker ","beat"+beat);
				if (beat == 0)
					audioTrack.write(tic,0,tic.length);
				else
					audioTrack.write(toc,0,toc.length);
				beat++;
			}
		else if (bar > 1.) { //bar is dec >1
			//When beat hits 0 we need to play the partial toc then the tic. This means that the tic will only be played after a partial toc so before we enter the loop, we need to play a tic.
			int partial = (int) ((toc.length)*(bar%1));
			audioTrack.write(tic,0,tic.length);
			beat++;
			while(isRunning) {
				beat %= (int) bar;
				if (beat != 0)//Common beat
					audioTrack.write(toc,0,toc.length);
				else {//Partial last beat + 1st beat
					audioTrack.write(toc,0,partial);
					audioTrack.write(tic,0,tic.length);
				}
				beat++;
			}
		}
		else if (bar == 1.) //Play only tic
			while(isRunning) audioTrack.write(tic,0,tic.length);
		else if (bar == 0.) //Play only toc
			while(isRunning) audioTrack.write(toc,0,toc.length);		
		
		else 
			while(isRunning) { //Partial tic. Stupid.
				int partial = (int) ((tic.length)*bar);
				audioTrack.write(tic,0,partial);
			}
	}
}
