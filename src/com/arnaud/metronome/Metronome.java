/** \mainpage Presentation
* Metronome.java is the main class. When Start is pressed, it calls Clicker.java or ClickerStructure.java depending on the mode chosen. The sounds arrays are initialisd through Signal. Then the main loop is run in a separate thread.<br>
* MenuSettings.java uses SharedPreferences so the 2 clickers classes can set the audioTrack and their signal from it.<br>
* MenuStructures.java exchange data wich Metronome.java through intents.<br>
* StructureContainer.java is a commodity class that encapsulates all the data for structures and deals with I/O.
*/

/** \file Metronome.java
* Main class.
*/

package com.arnaud.metronome;

import android.app.Activity;
import android.os.Bundle;

import android.os.AsyncTask;
import android.media.AudioManager; //Volume control through hardware button
import android.content.Intent; //Pour ouvrir d'autres activity: settings/pattern
import android.widget.Toast; //Pour afficher des trucs
import android.os.SystemClock; //Pour chronometrer le Tap In
import java.util.List;
import java.util.ArrayList;

/*Menu*/
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
/*UI*/
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;

import android.util.Log;

/** Main class
*/
public class Metronome extends Activity
{
    private long lastTapIn; /**< needed to compute the bpm */
    private int mode; /**< Metronome current state. -1: stopped, 0: normal clicker, >0: structure clicker */
    private StructureContainer container;
     
    private Clicker clicker;
    private ClickerTask clickerTask;
    
    private ClickerStructure clickerStructure;
    private ClickerStructureTask clickerStructureTask;
    
    private EditText editTextBpm;
    private EditText editTextBar;
    private ImageButton imageButtonStartStop;
    private Spinner spinnerMode;
    List<String> listStructure = new ArrayList<String>();
    /* Constantes */
    /* Les IDs pour le menu */
    private final int ID_MENU_SETTINGS = 1;
    private final int ID_MENU_STRUCTURES = 2;
    /* Run Mode */
    private final int NO_TASK = -1; /**< Paused */
    private final int CLICKER_TASK = 0; /**< Basic mode. Clicker.java used. If mode > CLIKER_TASK => ClickerStructure.java used. */

    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
	
	//Build UI and enable volume control
	editTextBpm = (EditText) findViewById(R.id.editTextBpm);
	editTextBar = (EditText) findViewById(R.id.editTextBar);
    	imageButtonStartStop = (ImageButton) findViewById(R.id.imageButtonStartStop);
    	
    	spinnerMode = (Spinner) findViewById(R.id.spinnerMode);
    	StructureContainer instance = new StructureContainer();
    	container = instance.loadFromInternal(this);
	int structuresCount = container.structures.length;
    	listStructure.add(getResources().getString(R.string.listStructure_normal));
    	for (int i=1;i<structuresCount+1;i++)
    		listStructure.add(String.format(getResources().getString(R.string.listStructure_structure),i));
    	ArrayAdapter<String> adapterSpinnerMode = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listStructure);
    	adapterSpinnerMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	spinnerMode.setAdapter(adapterSpinnerMode);
	spinnerMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		@Override
    		public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    			if (position != CLICKER_TASK) {
    				editTextBpm.setText("100");
    				editTextBar.setText("0");
    				Toast.makeText(getApplicationContext(),R.string.toastStructureMode,Toast.LENGTH_LONG).show();
    			}
    			if (position == CLICKER_TASK) {
    				editTextBpm.setText("120");
    				editTextBar.setText("4");
    				//Toast.makeText(getApplicationContext(),"BPM is back to normal use",Toast.LENGTH_SHORT).show();It shows at startup, not good.
    			}
    		}
    		@Override
    		public void onNothingSelected(AdapterView<?> arg0) {
    		}
    	});
	
	setVolumeControlStream(AudioManager.STREAM_MUSIC);
    	mode = NO_TASK;
    }
    
    /* The task handling Clicker's main loop. */
    private class ClickerTask extends AsyncTask<Void, Void, Void> {
	protected Void doInBackground(Void... params) {clicker.run(); return null;}
    }
    
    /* The task handling ClickerStructure's main loop. */
    private class ClickerStructureTask extends AsyncTask<Void, Void, Void> {
	protected Void doInBackground(Void... params) {clickerStructure.run(); return null;}
    }
    
    /** Build the menu
    * menu.xml show an empty so we fille it ourself
    */
    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
    	/*
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu,menu);
    	*/
    	
    	//menu.add(group.id,item id,order,title)
    	menu.add(Menu.NONE,ID_MENU_STRUCTURES,Menu.NONE,R.string.menuStructures);
    	menu.add(Menu.NONE,ID_MENU_SETTINGS,Menu.NONE,R.string.menuSettings);
    	return true;
    }
    
    /** Set new activities for options, feed them current data */
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
	switch (item.getItemId()) {
		case ID_MENU_SETTINGS:
			Intent intentMenuSettings = new Intent(Metronome.this, MenuSettings.class);
			startActivity(intentMenuSettings);
			return true;
		case ID_MENU_STRUCTURES:
			Intent intentMenuStructures = new Intent(Metronome.this, MenuStructures.class);
			intentMenuStructures.putExtra("Container",container);
			startActivityForResult(intentMenuStructures,ID_MENU_STRUCTURES);
			return true;
		default:
			return super.onOptionsItemSelected(item);
	}
    }
    /** And grab back edited data. MenuSettings won't return any intent. */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	super.onActivityResult(requestCode, resultCode, data);
    	switch (requestCode) {
    		case ID_MENU_STRUCTURES:
    			if(resultCode == 1) {
				container = (StructureContainer)data.getSerializableExtra("Container");
				int structuresCount = container.structures.length;
				listStructure.clear();
				listStructure.add(getResources().getString(R.string.listStructure_normal));
				if (structuresCount != 0) {
					spinnerMode.setSelection(1);//Selection 1st structure
					for(int i=1;i<structuresCount+1;i++)
						listStructure.add(String.format(getResources().getString(R.string.listStructure_structure),i));
				}
				else
					spinnerMode.setSelection(CLICKER_TASK);//In case the previously selected structure has been deleted.
			}
    			return;
    	}
    }

    /* Buttons Handlers. */
    public void onTapIn (View v)
    {// If using SystemClock.elapsedRealtimeNanos() (API 17), you need to adapt marked lines.
    	long delay, currentTapIn;
    	double bpm;
    	
    	currentTapIn = SystemClock.elapsedRealtime();//<--
    	delay = currentTapIn - lastTapIn;
    	lastTapIn = currentTapIn;

    	bpm = 60000./delay;//<--60000000000/delay if nanotime
    	editTextBpm.setText(String.valueOf(bpm));
    }
    
    /** Toggle the clicker depending on the current state \var mode */
    public void onStartStop (View v)
    {
	if (mode == NO_TASK) {//Go!
		/** UI stuff */
		String stringBpm = editTextBpm.getText().toString();
		String stringBar = editTextBar.getText().toString();
		if (stringBpm.matches("") || stringBar.matches("")) {
			Toast.makeText(getApplicationContext(),R.string.toastEnterSomeValue,Toast.LENGTH_SHORT).show();
			return;
		}
		
		double bpm = Double.parseDouble(stringBpm);
		double bar = Double.parseDouble(stringBar);
		if (bpm <= 0) {
			Toast.makeText(getApplicationContext(),R.string.toastBpmCantBeZero,Toast.LENGTH_SHORT).show();
			return;
		}

		imageButtonStartStop.setImageResource(android.R.drawable.ic_media_pause);
		imageButtonStartStop.setBackgroundColor(0xFFFF0000);
		//Clear focus
		editTextBpm.setEnabled(false);
		editTextBpm.setEnabled(true);
		editTextBar.setEnabled(false);
		editTextBar.setEnabled(true);

		/** Clicker Stuff */
		mode = spinnerMode.getSelectedItemPosition();
		//Setting clicker (audiotrack) and the thread for his run method
		if (mode == CLICKER_TASK) {//Normal Clicker
			clicker = new Clicker();
			clicker.on(bpm,bar,getApplicationContext());
			clickerTask = new ClickerTask();
			//Click!!
			clicker.setIsRunning(true);
			clickerTask.execute();
		}
		else {//Structure Clicker
			clickerStructure = new ClickerStructure();
			clickerStructure.on(container,mode-1,bpm,bar,getApplicationContext());
			clickerStructureTask = new ClickerStructureTask();
			//Click!!
			clickerStructure.setIsRunning(true);
			clickerStructureTask.execute();
		}
	}
	else {//Stop!
    		imageButtonStartStop.setImageResource(android.R.drawable.ic_media_play);
		imageButtonStartStop.setBackgroundColor(0xFF00FF00);
		//Clear focus
		editTextBpm.setEnabled(false);
		editTextBpm.setEnabled(true);
		editTextBar.setEnabled(false);
		editTextBar.setEnabled(true);

		//The cancel call won't kill the loop. It waits for it to end. clicker.off shuts down audio but the loop is kept in background.
		//We need to end the loop, then close the thread.
		//The spinnerMode is only read when starting so even if we changed the spinner selection while clicking, mode is still unchanged on stop. That's why we are able to determine the class to shutdown between clicker or clickerStructure.
		if (mode == CLICKER_TASK) {
			clicker.setIsRunning(false);
			clickerTask.cancel(true);
			//Unsetting clicker (audiotrack mostly)
			clicker.off();
		}
		else {
			clickerStructure.setIsRunning(false);
			clickerStructureTask.cancel(true);
			//Unsetting clicker (audiotrack mostly)
			clickerStructure.off();
			container.resetAudio();
		}
		mode = NO_TASK;
	}

	return;
    }

    public void onQuit (View v)
    {
    	finish();
	System.exit(0);
    }
}
