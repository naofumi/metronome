/** \file ClickerStructure.java */

package com.arnaud.metronome;

import android.content.Context;
import android.content.SharedPreferences;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
//import android.util.Log;

/** It's basically a Clicker who travel through a structure while settings signal and while clicking */
public class ClickerStructure
{
	private Structure structure;
	private Pattern[] patterns;
	private Base[] bases;
	private double structureSpeed;//Global adjustable bpm
	private double structureIter;//Number of times structure is getting played
	
	private float ticFreq, tocFreq;
	private int ticChoice, tocChoice, ticLength, tocLength;
	private boolean isRunning;
	
	private int sampleRate;
	private AudioTrack audioTrack;
	
	public void on(StructureContainer container, int structureIndex, double structureSpeed, double structureIter, Context context)
	{	
		SharedPreferences settings = context.getSharedPreferences("settings", 0);
		this.ticChoice = settings.getInt("tic",0);
		this.tocChoice = settings.getInt("toc",0);
		this.ticFreq = settings.getFloat("ticFreq",880);
		this.ticLength = settings.getInt("ticLength",50);
		this.tocFreq = settings.getFloat("tocFreq",660);
		this.tocLength = settings.getInt("tocLength",50);
		this.sampleRate = settings.getInt("sampleRate",44100);
		
		this.structure = container.structures[structureIndex];
		this.patterns = container.patterns;
		this.bases = container.bases;
		
		this.structureSpeed = structureSpeed;//From BPM
		this.structureIter = structureIter;//From Bar
		
		/*Audio init*/
		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, 2*sampleRate, AudioTrack.MODE_STREAM); /**< Buffer is set twice the size of sampleRate so the loop will always be feeding the buffer in reasonable advance */
		audioTrack.play();
		
		/*Set Sounds*/
		for(int i=0;i<structure.patterns.length;i++) {
			int patternIndex = structure.patterns[i].index;
			if (!patterns[patternIndex].isSetup) {
				for(int j=0;j<patterns[patternIndex].bases.length;j++) {
					int baseIndex = patterns[patternIndex].bases[j].index;
					if (!bases[baseIndex].isSetup) {
						double barBpm = bases[baseIndex].bpm * structureSpeed/100;//This allows control of global speed accross the structure with the global bpm (as a percentage).
						bases[baseIndex].tic = Signal.getPCM(context,ticChoice,ticFreq,ticLength,sampleRate,barBpm);
						bases[baseIndex].toc = Signal.getPCM(context,tocChoice,tocFreq,tocLength,sampleRate,barBpm);
						bases[baseIndex].isSetup = true;
					}
				}
				patterns[patternIndex].isSetup = true;
			}
		}
		
		isRunning = false;
	}
	
	public void off()
	{
		audioTrack.stop();
		audioTrack.release();
	}
	
	public void setIsRunning (boolean isRunning)
	{
		this.isRunning = isRunning;
	}
	
	public void run()
	{
		byte[] tic;
		byte[] toc;
		double bar;
		
		structureIter = (int) structureIter;//To deal with non integer value
		if (structureIter == 0)//infinite loop
			structureIter = -1;
		
		while(structureIter != 0) {
			for(int i=0;i<structure.patterns.length;i++) {
			int patternIndex = structure.patterns[i].index;
			int patternTimes = structure.patterns[i].times;
			for(int j=0;j<patternTimes;j++) {
				for(int k=0;k<patterns[patternIndex].bases.length;k++) {
				int baseIndex = patterns[patternIndex].bases[k].index;
				int baseTimes = patterns[patternIndex].bases[k].times;
				tic = bases[baseIndex].tic;
				toc = bases[baseIndex].toc;
				bar = bases[baseIndex].bar;
				for(int l=0;l<baseTimes;l++) {

					int beat = 0;
					
					if (bar == 1.) //Play only tic
						audioTrack.write(tic,0,tic.length);
					else if (bar == 0.) //Play only toc
						audioTrack.write(toc,0,toc.length);		
					else if (bar%1 == 0.)//bar is "int". Common use.
						while(beat < bar && isRunning) {
							if (beat == 0)
								audioTrack.write(tic,0,tic.length);
							else
								audioTrack.write(toc,0,toc.length);
							beat++;
						}
					else if (bar > 1.) { //bar is dec >1
						int partial = (int) ((toc.length)*(bar%1));
						audioTrack.write(tic,0,tic.length);
						beat++;
						while(beat < bar && isRunning) {
							if (beat < (int) bar)//Common beat
								audioTrack.write(toc,0,toc.length);
							else
								audioTrack.write(toc,0,partial);
							beat++;
						}
					}
					else {
						int partial = (int) ((tic.length)*bar);
						audioTrack.write(tic,0,partial);
					}
				}
				}
			}
			}
			
			structureIter--;
		}
	}
}
