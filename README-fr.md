# Metronome

Metronome est une application Android gratuite qui permet de jouer sur une gamme de BPM très large, garanti régulier et pouvant suivre une structure.

## Fonctionalités

Metronome a été créé afin de réunir plusieurs caractéristiques en une seule appli:

* gratuit et open-source
* BPM allant de ~0.2 à ~120000 (dépend du téléphone)
* gestion des mesures non-entières (à virgule)
* un clic régulier. Si votre téléphone peut lire un fichier audio sans broncher alors ce métronome sera régulier
* possibilité d'écrire une structure complexe (changements de métriques, équivalences, claves)

## Installation

Télécharger le .apk à partir de f-droid puis installer. Veiller à autoriser l'installation à partir de sources inconnues.

## Compilation

Deux possibilités:

    $ ant debug
    ou
    $ ant release

* Debug: Nécessite android-sdk (à partir de google). Éditer local.properties pour faire pointer vers android-sdk
* Release: Nécessite une clé en plus. Éditer ant.properties pour préciser le nom, l'alias et les mot de passe de la clé

## Documentation

Dependances: doxygen et pandoc

    $ cd doc
    $ make reference
    $ make manual

* Doc de référence: reference/html/index.html
* Manuel utilisateur: manual/manual-fr.html

## Support et retours

Vous pouvez me faire part de vos impressions et retours d'expérience pour les autres versions: funkygoby [] mailoo.org

Metronome est supporté sur les versions Android allant de JellyBean 4.3.1 à MarshMallow 6.1. Les versions plus récentes ne devraient pas poser de problème.

Metronome fonctionne sur les vieilles versions android mais avec quelques soucis (voir ci dessous).

### Problème connus

Metronome a été testé sur Andoid 4.x et 2.x. Cependant, un bug curieux apparait au bout de plusieurs cycles start/stop. Selon les versions d'Android, l'appli se ferme, ou ne produit plus de son. Android ne rapporte pas l'appli comme ayant planté.

Metronome reste disponible pour ces vieilles versions Android (ça ne mange pas de pain) mais la résolution de ce bug risque de rester en suspens.

## Licence

This whole project, exepted the raw files, is licensed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007. See LICENSE for more details.